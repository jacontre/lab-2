/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (jacontre): write class javadoc
 *
 * @author jacontre
 */
public class Kelvin extends Temperature {
	public Kelvin( float t )
	{
	super( t ) ;
	}
	public String toString( )
	{
	
		String h = String.valueOf(this.getValue());
		
		return h +" K";
	}
	@Override
	public Temperature toCelsius( ) {
		return new Celsius( this.getValue() - 273 );
	}
	@Override
	public Temperature toFahrenheit( ) {
	// TODO: Complete this method
	System.out.println("int: " + ( (this.getValue() - 273.15) * (9/5f) ) + 32 );
	return new Fahrenheit( ( (this.getValue() - 273.15f) * (9/5f) ) + 32 );
	}
	@Override
	public Temperature toKelvin() {
		return this;
	}
}
