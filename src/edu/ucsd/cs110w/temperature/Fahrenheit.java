/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author jacontre
 *
 */
public class Fahrenheit extends Temperature
{
	public Fahrenheit(float t)
	{
		super(t);
	}
	public String toString()
	{
		String h = String.valueOf(this.getValue());
		
		// TODO: complete method
		return h + " F";
	}
	@Override
	public Temperature toCelsius() {
		return new Celsius( ( this.getValue() - 32 ) * (5/9f) );
	}
	@Override
	public Temperature toFahrenheit() {
		return this;
	}
	@Override
	public Temperature toKelvin() {
		return new Kelvin( ((5/9f) * (this.getValue() - 32) + 273));
	}
}
