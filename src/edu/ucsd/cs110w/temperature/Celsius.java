/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author jacontre
 *
 */
public class Celsius extends Temperature
{
	public Celsius(float t)
	{
		super(t);
	}
	public String toString()
	{
		
		String h = String.valueOf(this.getValue());
		
		// TODO: complete method
		return h + " C";
	}
	@Override
	public Temperature toCelsius() { 
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		return new Fahrenheit( ((9/5f) * this.getValue()) + 32);
	}
	@Override
	public Temperature toKelvin() {
		return new Kelvin (this.getValue() + 273);
	}
}
